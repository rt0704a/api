# Utilisez l'image de base Python 3
FROM python:3

# Définir le répertoire de travail dans le conteneur
WORKDIR /app

# Copiez les fichiers nécessaires de l'hôte dans le conteneur
COPY app/ .
COPY requirements.txt requirements.txt

# Installez librarieqs
RUN pip install -r requirements.txt

# Exposez le port utilisé par l'application Flask
EXPOSE 5000

# Commande par défaut pour démarrer l'application Flask
CMD ["python", "main.py"]