from flask import Blueprint,jsonify,request
from werkzeug.security import check_password_hash
import os,json
users_bp = Blueprint('users', __name__)
user_file = "users.json"
#Gère les users et leur ludothèque
@users_bp.route('/get_user',methods=['POST'])
def get_user():
    data = request.get_json()
    user_id = data.get('user_id')

    # Construire le chemin vers le fichier JSON
    json_file_path = os.path.join('data', 'Users.json')
    # Vérifier si le fichier JSON "Users.json" est présent, si oui lire le contenu
    if os.path.exists(json_file_path):
        with open(json_file_path, 'r') as json_file:
            users = json.load(json_file)

            # Vérifiez si l'utilisateur avec l'ID donné est présent dans le JSON
            if user_id in users:
                user_data = users[user_id]
                return jsonify(user_data)

    # Si l'utilisateur n'est pas trouvé, retournez une réponse appropriée
    return jsonify({'error': 'User not found'}), 404


@users_bp.route('/signup_user',methods=['POST'])
def signup_user():
    data = request.json
    first_name = data.get('first_name',0)
    last_name = data.get('last_name',0)
    email = data.get('email',0)
    hashed_password = data.get('password',0)

    json_file_path = os.path.join('data', 'Users.json')

    if os.path.exists(json_file_path):
        with open(json_file_path, 'r') as json_file:
            users = json.load(json_file)
    
            # Incrémenter le dernier user_id existant
            user_id = str(int(max(users, key=int)) + 1)

            # Ajouter les informations de l'utilisateur au dictionnaire
            users[user_id] = {
                'first_name': first_name,
                'last_name': last_name,
                'email': email,
                'password': hashed_password
            }
        # Écrire le dictionnaire mis à jour dans le fichier JSON
        with open(json_file_path, 'w') as json_file:
            json.dump(users, json_file, indent=4)

        # Renvoyer l'ID de l'utilisateur ajouté dans la réponse JSON
        return jsonify({'user_id': user_id})

    # Renvoyer une réponse vide si le fichier JSON n'existe pas
    return jsonify({'user_id': None})


@users_bp.route('/login_user', methods=['POST'])
def login_user():
    data = request.json
    email = data.get('email',0)
    password = data.get('password',0)

    json_file_path = os.path.join('data', 'Users.json')

    if os.path.exists(json_file_path):
        with open(json_file_path, 'r') as json_file:
            users = json.load(json_file)

            user_id = None
            #Parcour à la recherche de l'adresse e-mail
            for uid, user_data in users.items():
                if 'email' in user_data and user_data['email'] == email:
                    user_id = uid
                    break

            # Si l'adresse e-mail est trouvée, vérifier le mot de passe
            if user_id is not None:
                stored_password_hash = user_data.get('password', None)
                if stored_password_hash is not None and check_password_hash(stored_password_hash, password):
                    # Connexion réussie, renvoyer les infos de l'utilisateur
                    first_name = user_data.get('first_name', None)
                    last_name = user_data.get('last_name', None)
                    return jsonify({'login_is_correct': True,'user_id': user_id,'first_name': first_name,'last_name': last_name,'password': stored_password_hash})
                else:
                    # Mot de passe incorrect
                    return jsonify({'login_is_correct': False})
            else:
                # Adresse e-mail non trouvée
                return jsonify({'login_is_correct': False})
    else:
        # Fichier JSON non trouvé
        return jsonify({'error': 'Fichier Users.json non trouvé'}), 500


@users_bp.route('/check_email', methods=['POST'])
def check_mail():
    data = request.get_json('email')
    email = str(data.get('email'))

    json_file_path = os.path.join('data', 'Users.json')

    if os.path.exists(json_file_path):
        with open(json_file_path, 'r') as json_file:
            users = json.load(json_file)

            # Vérifier si l'email est présent dans le JSON
            for user_id, user_data in users.items():
                if 'email' in user_data and user_data['email'] == email:
                    return jsonify({'email_already_use': True})
    return jsonify({'email_already_use': False})

@users_bp.route('/show_videotheque', methods=['POST'])
def show_videotheque():
    dataSearch = request.get_json()
    idUserConnected = dataSearch.get('user_id')
    json_file_path = os.path.join('data', 'Videotheque.json')
    if os.path.exists(json_file_path):
        with open(json_file_path, 'r') as json_file:
            videotheque = json.load(json_file)
            # Vérifiez si l'utilisateur avec l'ID donné est présent dans le JSON
            try:
                searchIfMovieExiste = videotheque[f'{idUserConnected}']
                return searchIfMovieExiste
            except KeyError:
                return jsonify({
                    'error': True
                })