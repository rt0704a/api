from flask import Flask, render_template
from flask import jsonify
from api import api_home_bp
from users import users_bp

app = Flask(__name__)
app.register_blueprint(api_home_bp, url_prefix='/api')
app.register_blueprint(users_bp, url_prefix='/user')
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
