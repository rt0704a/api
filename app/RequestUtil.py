import requests
from enum import Enum

class HttpMethod(Enum):
    """
    Une classe qui représente les requête HTTP possibles via une énumération
    
    Attributes
    ----------
    GET : str
    POST : str
    PUT : str
    DELETE : str
    """
    GET = "GET"
    POST = "POST"
    PUT = "PUT"
    DELETE = "DELETE"

class RequestUtil:
    """
    Une classe qui représente une requête HTTP
    
    Attributes
    ----------
    base_url : str
        URL de base pour la requête. Ex: http://10.11.3.209:5001/
    default_headers : list
        Headers par défaut à utiliser pour la requête, peut être remplacé après
    
    Methods
    -------
    make_request → Response
        method : str
            Quelle méthode utilisé pour faire la requête (GET,POST ...etc)
        endpoint : str
            Chemin d'accès pour la requête. Ex: /movies/search
        data : list
            Data à donner pour une requête POST. Ex un formulaire
        params : list
            Paramètres à donner pour la requête.
        headers : list
            header pour la requête. Peut être instancié lors de la création de l'objet
    """
    def __init__(self, base_url, default_headers=None):
        self.base_url = base_url
        self.default_headers = default_headers or {}
    def make_request(self, method, endpoint, data=None, params=None, headers=None):
        url = f"{self.base_url}{endpoint}"
        headers = {**self.default_headers, **(headers or {})}
        response = None

        try:
            if method == HttpMethod.GET:
                response = requests.get(url, params=params, headers=headers)
            elif method == HttpMethod.POST:
                response = requests.post(url, data=data, params=params, headers=headers)
            elif method == HttpMethod.PUT:
                response = requests.put(url, data=data, params=params, headers=headers)
            elif method == HttpMethod.DELETE:
                response = requests.delete(url, params=params, headers=headers)

        except requests.exceptions.RequestException as e:
            print(f"Request failed: {e}")
            if response:
                print(f"Response Content: {response.text}")
            return None

        return response