from flask import Blueprint,request,jsonify
from flask_login import current_user
from RequestUtil import RequestUtil, HttpMethod
from dotenv import load_dotenv
from movie import Movie
import os
import json
url_for_image = "https://image.tmdb.org/t/p/w220_and_h330_face"
api_home_bp = Blueprint('api', __name__)
#Recupérer variables du .env
load_dotenv()
TMDB_base_url: str = os.getenv('TMDB_BASE_URL')
TMDB_search_movie: str = os.getenv('TMDB_SEARCHMOVIE')
TMDB_api_key: str = os.getenv('TMDB_API_KEY')
TMDB_details: str = os.getenv('TMDB_DETAILS')
#Headers pour la requête
headers = {
    'accept': 'application/json',
    'Authorization': TMDB_api_key
}
#Paramètres pour rechercher un film, on peut réduire ou augmenter
paramsForMovies = {
    'query': '',
    'include_adult': 'false',
    'language': 'en-US',
    'page': 1,
    'region': '',
    'primary_release_year': '',
    'year': ''
}
@api_home_bp.route('/tmdb', methods=['POST'])
def tmdb():
    '''
    Using
    ----------
        http://ip_machine:5001/api/tmdb
        
    Parameters
    ----------
        name : str
            Nom du film à rechercher
        
    Returns
    ----------
        answer (json): requête reçu de The Movie Database
    '''
    dataSearch = request.get_json()
    name = dataSearch.get('search') #Récupère les arguments de la requête GET
    paramsForMovies['query'] = name #Remplace le nom du film dans la list
    requestToTMDB = RequestUtil(base_url=TMDB_base_url,default_headers=headers)
    answer = requestToTMDB.make_request(method = HttpMethod.GET,endpoint = TMDB_search_movie,params=paramsForMovies)
    json = answer.json()
    return json['results']
    #Réponse en json de la requête
@api_home_bp.route('/add_movie', methods=['POST'])
def add_movie():
    listCredits = []
    data = request.get_json()
    movieIDGetByRequest = data.get('movie_id')
    current_userID = data.get('user_id')
    detailsJSON = searchDetails(movieID=movieIDGetByRequest)
    creditsJSON = searchCredits(movieID=movieIDGetByRequest)
    cast = creditsJSON['cast']
    crew = creditsJSON['crew']
    for key in cast:
        if key["known_for_department"] == "Acting":
            if len(listCredits) < 3:
                listCredits.append(key['name'])
    for key2 in crew:
        if key2["known_for_department"] == "Production" and key2['job'] == "Producer":
                producer = key2['name']
    myMovie = Movie(id = movieIDGetByRequest,overview = detailsJSON['overview'],poster_path = f"{url_for_image}{detailsJSON['poster_path']}",release_date = detailsJSON['release_date'],title = detailsJSON['title'],genre = detailsJSON['genres'][0]['name'],runtime = detailsJSON['runtime'],vote_average = int(detailsJSON['vote_average']/2),actors = listCredits,director=producer,support="aucun")
    # Construire le chemin vers le fichier JSON
    json_file_path = os.path.join('data', 'Videotheque.json')
    videotheque = readDataFromJSON(json_file_path=json_file_path)
            # Vérifiez si l'utilisateur avec l'ID donné est présent dans le JSON
    try:
        searchIfMovieExiste = videotheque[f'{current_userID}']
        for film in searchIfMovieExiste:
            if film['id'] == movieIDGetByRequest:
                return jsonify({
                    'dejaExistant': True
                })
        videotheque[f'{current_userID}'].append(myMovie.__dict__)
    except KeyError:
        videotheque[f'{current_userID}'] = []
        videotheque[f'{current_userID}'].append(myMovie.__dict__)
    sendDataFromJSON(json_file_path,videotheque)
    return jsonify({
        'realise': True
    })

@api_home_bp.route('/manual_add_movie', methods=['POST'])
def manual_add_movie():
    idPlusHaut = 2000000000
    data = request.get_json()
    current_userID = data.get('user_id')
    title = data.get('title')
    release_date = data.get('release_date')
    genre = data.get('genre')
    director = data.get('director')
    actors = data.get('actors')
    support = data.get('support')
    overview = data.get('overview')
    duration = data.get('duration')
    rating = data.get('rating')
    myMovie = Movie(id = None,overview = overview,poster_path = "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Question_mark_alternate.svg/1200px-Question_mark_alternate.svg.png",release_date = release_date,title = title,genre = genre,runtime = int(duration),vote_average = int(rating),actors = actors,director=director,support=support)
    # Construire le chemin vers le fichier JSON
    json_file_path = os.path.join('data', 'Videotheque.json')
    videotheque = readDataFromJSON(json_file_path=json_file_path)
            # Vérifiez si l'utilisateur avec l'ID donné est présent dans le JSON
    for movie in videotheque:
        for userVideo in videotheque[movie]:
            if userVideo['id'] > idPlusHaut:
                idPlusHaut = userVideo['id']
    myMovie.id = idPlusHaut+1
    try:
        searchIfMovieExiste = videotheque[f'{current_userID}']
        videotheque[f'{current_userID}'].append(myMovie.__dict__)
    except KeyError:
        videotheque[f'{current_userID}'] = []
        videotheque[f'{current_userID}'].append(myMovie.__dict__)
    sendDataFromJSON(json_file_path,videotheque)
    return jsonify({
        'realise': True
    })
    
@api_home_bp.route('/info_movie', methods=['POST'])
def info_movie():
    data = request.get_json()
    current_userID = data.get('user_id')
    movieID = data.get('movie_id')
    json_file_path = os.path.join('data', 'Videotheque.json')
    videotheque = readDataFromJSON(json_file_path=json_file_path)
            # Vérifiez si l'utilisateur avec l'ID donné est présent dans le JSON
    try:
        searchIfMovieExiste = videotheque[f'{current_userID}']
        for movie in searchIfMovieExiste:
            if movie['id'] == movieID:
                return movie
    except KeyError:
        return jsonify({
            'erreur': True
        })
@api_home_bp.route('/update', methods=['POST'])
def update():
    data = request.get_json()
    current_userID = data.get('user_id')
    movieID = data.get('movie_id')
    title = data.get('title')
    release_date = data.get('release_date')
    genre = data.get('genre')
    director = data.get('director')
    actors = data.get('actors')
    support = data.get('support')
    overview = data.get('overview')
    duration = data.get('duration')
    rating = data.get('rating')
    poster_path = data.get('poster_path')
    myMovieUpdated = Movie(id = int(movieID),overview = overview,poster_path = poster_path,release_date = release_date,title = title,genre = genre,runtime = int(duration),vote_average = int(rating),actors = actors,director=director,support=support)
    json_file_path = os.path.join('data', 'Videotheque.json')
    #print(f'{current_userID} / {movieID}',file=open("verif.log","w+"))
    UpdateMovie(json_file_path=json_file_path,current_userID=current_userID,movieID=int(movieID),myMovieUpdated=myMovieUpdated)
    return jsonify({
        'fait': True
    })
@api_home_bp.route('/delete', methods=['POST'])
def delete():
    data = request.get_json()
    current_userID = data.get('user_id')
    movieID = data.get('movie_id')
    json_file_path = os.path.join('data', 'Videotheque.json')
    deletedMovie(json_file_path=json_file_path,current_userID=current_userID,movieID=movieID)
    return jsonify({
        'fait': True
    })
#Fonction de l'api mais sans route
def searchCredits(movieID):
    requestToTMDBCredits = RequestUtil(base_url=TMDB_base_url,default_headers=headers)
    params = {
        'language': 'en-US'
    }
    answerOfTMDBCredits = requestToTMDBCredits.make_request(method = HttpMethod.GET,endpoint = f'{TMDB_details}/{movieID}/credits',params=params)
    return answerOfTMDBCredits.json() #Réponse en json de la requete

def searchDetails(movieID):
    requestToTMDB = RequestUtil(base_url=TMDB_base_url,default_headers=headers)
    answerOfTMDBDetails = requestToTMDB.make_request(method = HttpMethod.GET,endpoint = f'{TMDB_details}/{movieID}?language=en-US')
    return answerOfTMDBDetails.json() #Réponse en json de la requete
def deletedMovie(json_file_path,current_userID,movieID):
    videotheque = readDataFromJSON(json_file_path)
    # Vérifiez si l'utilisateur avec l'ID donné est présent dans le JSON
    for i, entry in enumerate(videotheque[f'{current_userID}']):
        if entry['id'] == movieID:
            del videotheque[f'{current_userID}'][i]
    sendDataFromJSON(json_file_path,videotheque)
def UpdateMovie(json_file_path,current_userID,movieID,myMovieUpdated):
    videothequeToUpdate = readDataFromJSON(json_file_path)
    # Vérifiez si l'utilisateur avec l'ID donné est présent dans le JSON
    for i, entry in enumerate(videothequeToUpdate[f'{current_userID}']):
        print(f"{entry}", file=open("testprint.log","w+"))
        if entry['id'] == movieID:
            videothequeToUpdate[f'{current_userID}'][i] = myMovieUpdated.__dict__
    sendDataFromJSON(json_file_path,videotheque=videothequeToUpdate)
def readDataFromJSON(json_file_path):
    if os.path.exists(json_file_path):
        json_file = open(json_file_path, 'r')
        data_videotheque = json.load(json_file)
        json_file.close()
        return data_videotheque
def sendDataFromJSON(json_file_path,videotheque):
    if os.path.exists(json_file_path):
        json_file = open(json_file_path, 'w')
        json_file.seek(0)
        json.dump(videotheque, json_file, indent=4)
        json_file.close()