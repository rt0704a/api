import json
class Movie():
    def __init__(self, id = None,overview = None,poster_path = None,release_date = None,title = None,genre = None,runtime = None,vote_average = None,actors = None,support = None,director = None):
        self.id = id
        self.overview = overview
        self.poster_path = poster_path
        self.release_date = release_date
        self.title = title
        self.genre = genre
        self.runtime = runtime
        self.vote_average = vote_average
        self.actors = actors
        self.support = support
        self.director = director